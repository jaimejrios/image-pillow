# Image Pillow

A small (but handy) python script to compress images from defined paths.

## How to Install

1) Install [Python3](https://wiki.python.org/moin/BeginnersGuide/Download) on your system.
2) Next, install [pip3](https://pip.pypa.io/en/stable/installation/).
3) Afterwards, install the [pillow](https://pypi.org/project/Pillow/) pip3 package.

## How to use Image Pillow

1) Clone the Image Pillow repo into `~/Downloads`, then `cd` into it.
2) Copy the images you want to compress into the `src_images/` folder.
3) Run the Image Pillow script with the command `python3 image_pillow.py`

- All images inside the `dist/images/` folder will be cleaned (deleted).
- Your newly compressed images will be copied to the `dist/images/` folder.
- All images stored inside the `src_images/` folder will be left unchanged.

### Optional
- The `--clean-only` (or `-c`) flag can be added to only clean images from the `dist/images/` folder.
- Image compression will be omitted.
- Image Pillow script can be executed with the `--clean-only` option like so:\
`python3 image_pillow.py --clean-only`

## How to change Image Pillow settings

Within the `image_pillow.py` script, there are global variables you can alter to change the following:

- Source path of the images to be compressed.
- Destination folder of the compressed images.
- The image scale factor value (default value is 0.6 - this means the compressed images will be 60% of their original size).
- The image formats you want to target.

### Changing variable values:

```python
src_path = os.getcwd() + '/src_images'
export_path = os.getcwd() + '/dist/images/'
img_scale_factor = 0.6
img_formats = ('jpg', 'png')
```

* `src_path` variable:
  - The `/src_images` string can be changed to define a new source path for your images.
  - Make sure to include a leading forward slash `/` in your string, or else the path will be invalid.
* `export_path` variable:
  - The `/dist/images/` string can be changed to define a new destination path for your compressed images.
  - Make sure to include a leading forward slash and backslash `/` in your string, or else the path will be invalid.
* `img_scale_factor` variable:
  - The decimal value can be changed to a number within the range of `.1 - 1`
  - For example, setting the `img_scale_factor` variable to a numerical value of `.4` will compress your images to 40% of their original size.
* `img_formats` variable:
  - Specifies the image formats you want to target within the defined source path.
  - Supports `jpg`, `png`, and `webp` formats.

