import PIL
from PIL import Image
import os, sys

src_path = os.getcwd() + '/src_images'
export_path = os.getcwd() + '/dist/images/'
img_scale_factor = 0.6
img_formats = ('jpg', 'png')

def compress_images(directory=False, quality=30, scale_factor=1):
    if directory:
        os.chdir(directory)

    files = os.listdir()
    images = [file for file in files if file.endswith(img_formats)]

    for image in images:
        print('Compressing image ' + image)
        img = Image.open(image)
        new_width = int(img.size[0] * scale_factor)
        new_height = int(img.size[1] * scale_factor)
        img = img.resize((new_width, new_height), Image.ANTIALIAS)
        img.save(export_path + image, optimize=True, quality=quality)
        #img.save(export_path + image + '.webp', 'webp', optimize=True, quality=quality)

    print('')

def clean_images(path):
    print('\nCleaning images in ' + path + '\n')
    files = os.listdir(path)
    for file in files:
        os.remove(path + file)

def main():

    if (len(sys.argv) > 1):
        if (sys.argv[1].lower() == "-c" or sys.argv[1].lower() == "--clean-only"):
            clean_images(export_path)
    else:
        clean_images(export_path)
        compress_images(src_path, 30, img_scale_factor)

main()
